### Running the pipeline

When everything is installed you can do a test run using:

`nextflow run nf-core/smrnaseq -profile test,singularity`
