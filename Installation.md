### Downloading and installing dependencies

	* Download and install Nextflow
	* Download and install Singularity (Please use singularity on servers, since docker needs sudo rights to be executed)


#### Nextflow ####
Make sure java is installed

`java -version`

Download nextflow and install

`curl -fsSL get.nextflow.io | bash`

Move nextflow to your path

`mv nextflow ~/bin/`

Make sure you can run Nextflow

`netflow run hello`

Typical output if nextflow is installed properly

`N E X T F L O W  ~  version 20.10.0`
`Launching nextflow-io/hello [sad_tesla] - revision: 96eb04d6a4 [master]`
`executor >  local (4)`
`[91/eb5b6f] process > sayHello (1) [100%] 4 of 4 ✔`
`Ciao world!`

`Hello world!`

`Hola world!`

`Bonjour world!`



#### Singularity ####
Update your system

`sudo apt-get update`
 
Get all dependencies
 
`sudo apt-get -y install build-essential curl git sudo man vim autoconf libtool`
 
Download singularity from github, this will create a folder singularity
 
`git clone https://github.com/singularityware/singularity.git`

Go to the singularity folder
`cd singularity`

Install singularity

`./mconfig`
`cd ./builddir`
`make`
`sudo make install`

Make sure singularity is installed

`singularity version`